// Contains all the functions and business logics of our application
const Task = require("../models/Task.js");

module.exports.getAllTasks = () => {

	/*
		Task.finddin({}) = 
		[{
			"_id": "642693a0a4b2f18e99c6f1a0",
	        "name": "Eat Dinner",
	        "status": "pending",
	        "__v": 0
		}]

		.then(result)
		result = [{
			"_id": "642693a0a4b2f18e99c6f1a0",
	        "name": "Eat Dinner",
	        "status": "pending",
	        "__v": 0
		}]

		Task.find({}).then
	*/
	return Task.find({}).then(result => {

		return result;
	})


};

module.exports.createTask = () => {

	return Task.findOne({name: requestBody.name}).then((result, error) => {

		if (result !== null && result.name == requestBody.name) {

			return "Duplicate Task Found"                                                                                 
		}
		else {

			let newTask = new Task ({

				name: requestBody.name
			})

			return newTask.save().then((savedTask, savedErr) => {

				if (savedErr) {

					console.log(savedErr)
					return "Task Creation Failed";


				}
				else {

					return savedTask;
				}
			})
		}
	})
}

module.exports.deleteTask = (paramsId) => {

	return Task.findByIdAndRemove(paramsId).then((removeTask, err) => {

		if (err) {

			console.log(err)
			return "Task was not removed";
		}
		else{

			return "Removed Task Successfully";
		}
	})
}

module.exports.updateTask = (paramsId, requestBody) => {

	return Task.findById(paramsId).then((result, err) => {

		if (err) {

			console.log(error);
			return "Error Found";
		}
		else{

			result.name = requestBody.namespace

			return result.save().then((updatedTask, err) => {

				if (error) {

					console.log(error);
					return false;
				}
				else{

					return updatedTask;
				}
			})
		}
	})
}

// Activity
// Create a controller function for getting a specific task
module.exports.getSpecificTask = (paramsId) => {

	return Task.findById(paramsId).then((result, err)  => {

		if (err) {

			console.log(err);
			return err
		}
		else {

			return result
		}
	});
};

// Create a controller function for changing the status of a task to complete
module.exports.changeStatusToComplete = (paramsId) => {

	return Task.findById (taskId).then((result, err) => {

		if (err) {

			console.log(err);
			return err;
		}
		else {

			return status = "complete";
			return result.save().then((updatedTaskStatus, savedErr) =>  {

				if(savedErr){

					console.log(savedErr);
					return false;
				}
				else{

					return updatedTaskStatus;
				}
			});
		}

	});
}