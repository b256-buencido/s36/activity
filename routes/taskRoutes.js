// Contains all the URI endpoints for our application
const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP methods middlewares that makes it easier to create routes for our application
const router = express.Router();
const taskController = require("../controllers/taskControllers.js")

// [SECTION] Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller fcuntions from the controller files
router.get("/", (request, response) => {

	taskController.getAllTasks().then(resultFromController => {response.send(resultFromController)});
});

router.post("/create", (request, response) => {

	taskController.createTask(request.body).then(resultFromController => {response.send(resultFromController)});
});

router.delete("/:id", (request, response) => {

	taskController.deleteTask(request.params.id).then(resultFromController => {response.send(resultFromController)});
})

router.put("/:id", (request, response) => {

	taskController.updateTask(request.params.id, request.body).then(resultFromController => {response.send(resultFromController)});
})

// Activity
// Create a route for getting a specific task.
router.get("/:id", (request, response) => {

	taskController.getSpecificTask(request.params.id).then(resultFromController => {response.send(resultFromController)});
});

// Create a route for changing the status of a task to complete
router.put("/:id/complete", (request, response) => {

	taskController.changeStatusToComplete(request.params.id).then(resultFromController => {response.send(resultFromController)});
});

module.exports = router;